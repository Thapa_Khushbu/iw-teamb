from django.contrib import admin
from social_core.tests.models import User

from groupbslack.models import AccountType, User_details

admin.site.register(AccountType)
admin.site.register(User_details)
# Register your models here.
