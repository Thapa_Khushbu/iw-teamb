from django.contrib.auth.models import User
from django.db import models

class AccountType(models.Model):
    type=models.CharField(max_length=50)

    def __str__(self):
        return self.type

class User_details(models.Model):
    image=models.ImageField(upload_to="profile", blank=True, null=True)
    billing_status=models.CharField(max_length=9)
    account_type=models.ForeignKey(AccountType, on_delete=models.CASCADE, default=3)
    user = models.ForeignKey(User,on_delete=models.CASCADE)

    # def __str__(self):
    #     return self.user
