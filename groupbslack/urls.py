from django.urls import path, include
from rest_framework import routers
from django.conf.urls.static import static

from groupbslack.views import manageTable, update_account, signin, register, logouts, sendInvitation, UserListView, \
    RegisterApiView, User_detailsViewSet
from slack import settings

router = routers.DefaultRouter()
router.register(r'userDetails', User_detailsViewSet)

urlpatterns = [
    path("signin/",signin, name="login"),
    path("register/",register, name="signup"),
    path("logout/",logouts, name="logout"),
    # path("google/",include('social_django.urls', namespace='social')),
    path("",manageTable,name="manage"),
    path("sendmail/",sendInvitation,name="sendInvitation"),
    path("update/<int:a>/",update_account),
    path('update/<int:a>/)',update_account),
    path('list-users/', UserListView.as_view(), name='list_user'),
    path('registers/', RegisterApiView.as_view(),name='register_view'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += router.urls

