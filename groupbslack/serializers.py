from django.contrib.auth.forms import UserModel
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from groupbslack.models import User_details


class UserSerializer(ModelSerializer):
    password= serializers.CharField(write_only=True)

    def create(self, validated_data):
        user=UserModel.objects.create(username=validated_data['username'])
        user.set_password(validated_data['username'])
        user.save()

        return user

    class Meta:
        model=UserModel
        fields=("username", "password")


class User_details_Serializer(ModelSerializer):
    class Meta:
        model=User_details
        fields='__all__'
