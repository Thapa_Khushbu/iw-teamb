from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserModel
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render, redirect
# from social_core.tests.models import User
from rest_framework import permissions
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from social_core.utils import user_is_authenticated

from slack import settings
from .models import User_details, AccountType, User
from .models import User_details, AccountType, User
from .serializers import UserSerializer, User_details_Serializer


#
# def signin(request):
#     return render(request, "home/Signin.html")

def signin(request):
    # if user_logged_in:
    #     return redirect("details/")
    if request.method=='POST':
        email=request.POST['email']
        usernames = User.objects.get(email=email)
        uid=usernames.id
        userdetails=User_details.objects.get(user_id=uid)
        if(userdetails.account_type_id == 5):
            messages.success(request, "Access denied.")
            return render(request, "home/Signin.html")
        # print(usernames.username)
        password=request.POST['password']
        user=authenticate(request,username=usernames.username, password=password)
        print(user)
        if user is not None:
            login(request,user)
            return redirect('/slack/')
        else:
            print("no user")

    return render(request, "home/Signin.html")

# def register(request):
#     return render(request, "home/signup.html")


def register(request):

    if request.method=='POST':
        firstName=request.POST['firstname']
        lastName=request.POST['lastname']
        email=request.POST['email']
        username=request.POST['username']
        password1=request.POST['password1']
        password2=request.POST['password2']
        file = request.FILES['file']

        password=make_password(password1, salt=None, hasher='default')
        user1=User(username=username,first_name=firstName,last_name=lastName,email=email,password=password,is_staff=1, is_superuser=1)
        # user1.set_password([password])
        # account_type=AccountType.objects.get(type=3)

        user1.save()
        user_detail=User_details(user=user1,image=file, billing_status='inactive')
        user_detail.save()
        return render(request, "home/Signin.html")
    return render(request, "home/signup.html")



@login_required(login_url='/slack/signin/')
def sendInvitation(request):
    if request.method=='POST':
        subject="Invitation mail."
        message="http://127.0.0.1:8000/slack/register/"
        from_email=settings.EMAIL_HOST_USER
        to_list=[request.POST['email'], settings.EMAIL_HOST_USER]
        send_mail(subject,message,from_email,to_list)
        return redirect("http://127.0.0.1:8000/slack/")
    return render(request, "manage_members/manage_index.html")

@login_required(login_url='/slack/signin/')
def manageTable(request):
    userid=request.user
    user_pic=User_details.objects.get(user=userid)
    account_type=AccountType.objects.all()
    users_data=User_details.objects.all()
    context={'users_data':users_data,'user_pic':user_pic,'account_type':account_type}
    return render(request, "manage_members/manage_index.html",context)

@login_required(login_url='/slack/signin/')
def update_account(request,a,*args,**kwargs):
    type=request.GET.get('type')
    user=User_details.objects.get(user_id=a)
    user.account_type_id=type
    user.save()
    userid = request.user.id
    if(type=='5' and userid == a ):
        # print(userid)
        logout(request)
        messages.success(request, "Your account has been deactivated.")
        return render(request, "home/Signin.html")
    return redirect("http://127.0.0.1:8000/slack/")
    # return HttpResponse("hello")

@login_required(login_url='/slack/signin/')
def logouts(request):
    logout(request)
    return render(request, "home/Signin.html")

def index(request):
    return render(request, "home/index.html")



class UserListView(ListAPIView):
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer
    model=UserModel


class RegisterApiView(CreateAPIView):
    queryset =User_details.objects.all()
    serializer_class = User_details_Serializer

    model=User_details
    permission_classes = [
        permissions.AllowAny
    ]

class User_detailsViewSet(ModelViewSet):
    queryset = User_details.objects.all()
    serializer_class = User_details_Serializer
    model=User_details
    permission_classes =[IsAuthenticated]

